# README #

This repository contains the source of R in Insurance 2015 Giorgio Spedicato presentation.

### What is this repository for? ###

* R in Insurance 2015 Code and PDF
* As of 22-06-2015
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Download R and RStudio
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* spedicato_giorgio at yahoo.it