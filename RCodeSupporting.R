#load the package and the italian tables
rm(list=ls())
workDir='G:\\giorgio lavoro\\universita\\RInInsurance2015'
setwd(workDir)
numsim=1000
library(xts)
library(lifecontingencies)
source("./Code/retrieveData.R") #load the data
source("./Code/financialHyphotheses.R") #thefine the baseline hp
source("./Code/demographicalSimulation.R") #performs the simulations
source("./Code/vasicekFunctions.R") #utility function to simulate Vasiceck interest rate scenarios
source("./Code/financialAnalysis.R") #function to model the interest rate dynamics and inflation with cointegration
source("./Code/pullingAllTogeter.R") #do the final simulations

