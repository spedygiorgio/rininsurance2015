#calibrate lee carter
italy.leecarter<-lca(data=italyDemo,series="total",max.age=103,adjust = "none")
#perform modeling of kt series
italy.kt.model<-auto.arima(italy.leecarter$kt)

#' @title projecting life tables
#' 
#' @description given a LeeCarted object estimated from demography returns a projected life table
#' allowing or not ("central estimate") for parameters uncertainty.
#' @param lcObj: a lee carter object
#' @param kt.model: a forecast package model for projecting mortality rates.
#' @param coort: the birth coort
#' @param type: if central, the expected value of kt stochastic process and epsilon (0) will be used otherwise a simulation
#' @param ltName: name of the resulting life table  


tableSimulator<-function(lcObj, kt.model=auto.arima(lcObj$kt), coort=1950, type="central", ltName="Baseline", rate) {
  require(demography)
  if(type=='central') kt.scenario<-forecast(kt.model,h=100)$mean else {
    kt.scenario<-simulate.Arima(kt.model, nsim = 100, future = T) #this will simulate 40 periods into the future
  }
  #indexing the kt
  kt.full<-ts(union(italy.leecarter$kt, kt.scenario),start=1872)  
  
  #generating the varepsilon (error) matrix
  
  if(type=='central') sdEpsilon<-0 else sdEpsilon<-sd(as.numeric(lcObj$residuals$y))
  matrEpsilon<-matrix(rnorm(n=length(lcObj$ax)*length(kt.full),0,sdEpsilon),nrow =length(lcObj$ax),ncol= length(kt.full))
  #getting and defining the life tables matrix
  mortalityTable<-exp(italy.leecarter$ax+italy.leecarter$bx%*%t(kt.full)+matrEpsilon) 
  rownames(mortalityTable)<-seq(from=0, to=103)
  colnames(mortalityTable)<-seq(from=1872,to=1872+dim(mortalityTable)[2]-1)
  
  #returning the life table obj
  
  colIndex<-which(colnames(mortalityTable)==coort) #identify #the projection is to be taken
  maxLength<-min(nrow(mortalityTable)-1,ncol(mortalityTable)-colIndex)
  qxOut<-numeric(maxLength+1)
  for(i in 0:maxLength)
    qxOut[i+1]<-mortalityTable[i+1,colIndex+i]
  #fix: we add a fictional omega age where 
  #death probability = 1
  qxOut<-c(qxOut,1)
  #now getting the life table
  ltOut<-probs2lifetable(probs=qxOut,type="qx",name=ltName)
  #if a rate is specified returns a rate
    if(missing(rate)) out<-ltOut else {
    out<-new("actuarialtable",x=ltOut@x,lx=ltOut@lx,interest=rate)
    }
  return(out)
}


#TESTING
at1950Baseline<-tableSimulator(lcObj= italy.leecarter, kt.model = italy.kt.model,coort=1950,type="simulated",ltName="Baseline",rate=realRate)
exn(object = at1950Baseline,x = 65)
axn(actuarialtable = at1950Baseline,x = 65,k=12)


#ASSESSING PROCESS VARIANCE
lifesim.pv<-rLife(n=numsim,object=at1950Baseline,x=65,k=12,type = "Kx")
annuitysim.pv<-rLifeContingencies(n=1e3,lifecontingency = "axn",object=at1950Baseline,x=65,k=12,parallel=TRUE)

processVarianceSimulations<-data.frame(remainingLifetime=lifesim.pv,annuityAPV=annuitysim.pv)

lifesimHist<-ggplot(processVarianceSimulations, aes(x=remainingLifetime)) + geom_histogram(aes(y = ..density..)) + labs(x="Kt",title="Remaining lifetime")
annuityHist<-ggplot(processVarianceSimulations, aes(x=annuityAPV)) + geom_histogram(aes(y = ..density..)) + labs(x="APV",title="Annuity Actuarial Present Value simulation")

#ASSESSING PROCESS + PARAMETERS VARIANCE
## generate N life table then sample from Kt and annuity distribution

# ###nesting process variance and parameter variance
# numsim.parvar=31
# numsim.procvar=31
# ### simulating parameter variance
# tablesList<-list()
# for(i in 1:numsim.parvar) tablesList[[i]] <- 
#   tableSimulator(lcObj = italy.leecarter,
#   kt.model = italy.kt.model,
#   coort = 1950,
#   type = 'simulated',
#   ltName = paste("table",i),
#   rate=realRate
# )
# ### simulating process variance
# lifesim.full<-as.numeric(sapply(X=tablesList, FUN="rLife",n=numsim.procvar,x=65,k=12,type = "Kx"))
# annuitysim.full<-as.numeric(sapply(X=tablesList, FUN="rLifeContingencies",n=numsim.procvar,x=65,k=12,lifecontingency = "axn"))
# 
# fullVarianceSimulations<-data.frame(remainingLifetime=lifesim.full,annuityAPV=annuitysim.full)
#save(list=c("fullVarianceSimulations","processVarianceSimulations"),file="./Data/remaininglifeSims.RData")
load(file="./Data/remaininglifeSims.RData")
#apply(fullVarianceSimulations,2,sd)/apply(processVarianceSimulations,2,sd)

